# webAdventurer

A script which follows links at random and prints where it currently is.
You may give it some directions on which links to follow.
An Internet explorer, if you will.

Installation and usage: Download adventurer.py, edit the parameters at the 
top of the file and run it.

Explanation of the parameters:

    startURL
        This is where the script starts following links. Here the script might
        end up again if its patience drops to zero.
        
    preferredURLBits
        If strings in this list are found from any of the found link URLs,
        such links are always followed. If no such links are found, a totally 
        random link is followed.
        
    unpreferredURLBits
        If strings in this list are found from any of the found link URLs,
        such links are never followed. If all links are unpreferred, a totally
        random link is followed.
        
    adventureTime
        The runtime of the script; how many links are followed in total.
        
    maxPatience
        Patience starts at maxPatience and it is decremented every time 
        HTTP or URL errors are encountered, no links are found from the page
        or the link takes the script back to the same page. This prevents the
        script from locking up.
        
    patienceRecoveryChance
        Patience is fully recovered when going back to startURL due to patience
        loss, but patience may be incrementally recovered and this parameter
        determines the chance of that happening at each link.
        
Notes:

    If a URL matches both preferredURLBits and unpreferredURLBits, it is never
    followed.
    
    