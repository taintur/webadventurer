#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 16:36:09 2018

@author: taintur
"""

import urllib.request as ureq 
from lxml import html
import random

startURL = "https://en.wikipedia.org/wiki/Main_Page"

preferredURLBits = ["https://en.wikipedia.org/wiki/"]
unpreferredURLBits = ["Help", "Special","edit"]

adventureTime = 50
maxPatience = 5
patienceRecoveryChance = 0.2

print("Started from " + startURL)

nextURL = startURL
prevURL = startURL

urlHistory = []
linkHistory = []

patience = maxPatience

def escapeDecision(start,prev,patience,maxPatience):
    patience = patience - 1
    print("Patience lost. Current patience: " + str(patience) + "/" + str(maxPatience))
    if patience < 1:
        print("I've had enough! Let's go back to the beginning!")
        return (start,maxPatience)
    elif patience == 1:
        print("Okay, let's go back a bit, but just once!")
        return (prev,patience)
    else:
        print("Okay, let's go back a bit.")
        return (prev,patience)

def handleURL(URL,rawLink):
    rawLinkSplit = rawLink.lstrip("/").split("/")
    URLSplit = URL.rstrip("/").split("/")
    if rawLink[0:4] == "http":
        return rawLink
    else:
        commonIdx = -1
        for i in range(len(URLSplit)):
            if URLSplit[i] == rawLinkSplit[0]:
                commonIdx = i
        if commonIdx != -1:
            newURL = "/".join(URLSplit[0:commonIdx] + rawLinkSplit)
        else:
            newURL = URL + rawLink
        tempSplit = newURL.split("/")
        returnSplit = []
        for i in range(len(tempSplit)):
            if i == 1 or tempSplit[i] != "":
                returnSplit.append(tempSplit[i])
        newURL = "/".join(tempSplit)
    return newURL

"""
Adventure starts here --v
"""
for i in range(adventureTime):
    
    #read the site data and catch 404 errors and such
    links = []
    try:
        pageContent = html.fromstring(ureq.urlopen(nextURL).read())
    except ureq.HTTPError:
        print("We encountered a HTTP Error Monster! "
              + "It looks like this: '" + nextURL + "'")
        (nextURL, patience) = escapeDecision(startURL,prevURL,patience,maxPatience)
        continue
    except ureq.URLError:
        print("Encountered an URL Error Monster!")
        (nextURL, patience) = escapeDecision(startURL,prevURL,patience,maxPatience)
        continue
    try:
        pageTitle = pageContent.find(".//title").text
        if type(pageTitle) == type(None):
            raise AttributeError
    except AttributeError:
        pageTitle = "Untitled page"
    print("")
    print("Arrived to '" + pageTitle + "'\n(" + nextURL + ").")

    #parse the page for links and make a list out of them
    for link in pageContent.xpath("//a"):
        urlString = link.get("href")
        if type(urlString) == type(None) or urlString == "":
            continue
        urlLink = handleURL(nextURL,urlString)
        if urlLink != None:
            links.append(urlLink)
    
    print("This place has " + str(len(links)) + " links!" + min((len(links) // 50),10) * "!")
    
    #check if the page had any links
    if not links:
        print("This is a dead end!")
        (nextURL, patience) = escapeDecision(startURL,prevURL,patience,maxPatience)
        continue
    
    #filter the links with preferations
    if preferredURLBits != []:
        tempLinks = []
        for i in range(len(links)):
            for prefBit in preferredURLBits:
                if prefBit in links[i]:
                    tempLinks.append(links[i])      
        if not tempLinks:
            links = links
            print("No preferred links found.")
            print("Picking a random link...")
        else:
            links = tempLinks
            print("Picking a random preferred link...")
    else:
        print("Picking a random link...")
    
    if unpreferredURLBits != []:
        tempLinks = []
        for i in range(len(links)):
            for unprefBit in unpreferredURLBits:
                if unprefBit not in links[i]:
                    tempLinks.append(links[i])
        if not tempLinks:
            links = links
            print("Only unpreferred links found.")
            print("Picking a random unpreferred link...")
        else:
            links = tempLinks
    
    #select a link to go to
    selection = random.randint(0,len(links)-1)
    prevURL = nextURL
    urlHistory.append(prevURL)
    linkHistory.append(len(links))
    nextURL = links[selection]
    if nextURL == prevURL:
        print("I'm trapped!")
        (nextURL, patience) = escapeDecision(startURL,prevURL,patience,maxPatience)
    if patience < maxPatience and random.random() < patienceRecoveryChance:
        patience = patience + 1
        print("Patience recovered. Current patience: " + str(patience) + "/"
              + str(maxPatience))
    
print("")
print("Adventure finished!")
print("I visited " + str(len(set(urlHistory))) + " unique pages!")
print("I saw " + str(sum(linkHistory)) + " links in total!")
print("Biggest amount of links I saw on a page was " + str(max(linkHistory)) + "!")